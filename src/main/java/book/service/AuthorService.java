package book.service;

import book.domain.Author;

import java.util.List;

public interface AuthorService {
    List<Author> getAll();
    Author getAuthorByName(String name);

    void save(Author author);

    void delete(Author author);
}
